import java.util.Arrays;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.List; //subtask 8

public class App {
    public static void main(String[] args) throws Exception {
        //subtask 1 Tạo một mảng gồm các phần tử có giá trị từ x đến y
        int x1 = -4;
        int y1 = 7;
        int[] result1 = new int[y1 - x1 + 1];
        for (int i = 0; i < result1.length; i++) {
            result1[i] = x1 + i;
        }
        System.out.println("Mang co gia tri di tu x den y: " + Arrays.toString(result1));

        //subtask 2 Gộp 2 mảng lại với nhau, bỏ hết các phần tử có giá trị trùng nhau
        Integer[] array21 = {1, 2, 3};
        Integer[] array22 = {1, 2, 3};

        Set<Integer> set = new HashSet<>();

        set.addAll(Arrays.asList(array21));
        set.addAll(Arrays.asList(array22));

        Integer[] result2 = set.toArray(new Integer[set.size()]);

        System.out.println("Mang sau khi gop va loai bo het gia tri trung nhau: " + Arrays.toString(result2));

        //subtask 3 Đếm số lần xuất hiện của phần tử n trong mảng
        int[] array3 = {1, 2, 1, 4, 5, 1, 1, 3, 1};
        int n3 = 1;
        int count3 = 0;
        for (int i = 0; i < array3.length; i++) {
            if (array3[i] == n3) {
                count3++;
            }
        }
        System.out.println("So lan xuat hien cua phan tu: " + n3 + " trong mang la: " + count3);

        //subtask 4 Tính tổng các phần tử trong mảng
        int[] numbers4 = {1, 2, 3, 4};
        int sum4 = 0;
        for (int number : numbers4) {
            sum4 += number;
        }
        System.out.println("Tong cac phan tu trong mang la: " + sum4);

        //subtask 5 Từ một mảng cho trước, tạo một mảng gồm toàn các số chẵn
        int[] originalArray5 = {1,2,3,4,5,6,7,8,9,10};
        ArrayList<Integer> evenNumbers5 = new ArrayList<Integer>();

        for (int i = 0; i < originalArray5.length; i++) {
            if (originalArray5[i] % 2 == 0) {
                evenNumbers5.add(originalArray5[i]);
            }
        }
        int[] evenArray5 = new int[evenNumbers5.size()];
        for (int i = 0; i < evenNumbers5.size(); i++) {
            evenArray5[i] = evenNumbers5.get(i);
        }
        System.out.println("Mang loc ra gom cac so chan: " + Arrays.toString(evenArray5));

        //subtask 6 Từ 2 mảng cho trước, tạo một mảng mới trong đó mỗi phần tử là tổng của 2 phần tử ở vị trí tương ứng của 2 mảng đã cho
        int[] arr61 = {1, 0, 2, 3, 4};
        int[] arr62 = {3, 5, 6, 7, 8, 13};
        int n6 = Math.max(arr61.length,arr62.length);
        int[] result6 = new int[n6];
        for (int i = 0; i < n6; i++) {
            int a6 = (i < arr61.length) ? arr61[i] : 0;
            int b6 = (i < arr62.length) ? arr62[i] : 0;
            result6[i] = a6 + b6;
        }
        System.out.println("Gia tri cua tung phan tu la tong cua 2 gia tri moi mang cho truoc: " + Arrays.toString(result6));

        //subtask 7 Từ 1 mảng cho trước tạo một mảng mới gồm các phần tử của mảng cũ nhưng không bao gồm các phần tử có giá trị trùng nhau
        int[] arr7 = {1, 2, 3, 1, 5, 1, 4, 6, 3, 4};
        Set<Integer> set7 = new LinkedHashSet<>();
        for (int i : arr7) {
            set7.add(i);
        }
        int[] result7 = new int[set7.size()];
        int i = 0;
        for (Integer num : set7) {
            result7[i++] = num;
        }
        System.out.println("Tao mang moi gom cac phan tu ko co gia tri trung nhau: " + Arrays.toString(result7));

        //subtask 8 Lấy những phần tử có giá trị không trùng nhau của 2 mảng cho trước
        Integer[] arr81 = {1, 2, 3};
        Integer[] arr82 = {100, 2, 1, 10};
        List<Integer> list81 = new ArrayList<>(Arrays.asList(arr81));
        List<Integer> list82 = new ArrayList<>(Arrays.asList(arr82));
        List<Integer> result8 = new ArrayList<>();
        
        for (Integer i81 : list81) {
            if (!list82.contains(i81)) {
                result8.add(i81);
            }
        }
        for (Integer i82 : list82) {
            if (!list81.contains(i82)) {
                result8.add(i82);
            }
        }
        System.out.println("Mang gom nhung phan tu khong trung nhau lay tu 2 mang cho truoc: " + result8);

        //subtask 9 Sắp xếp các phần tử theo giá trị giảm dần
        int[] arr9 = {1, 3, 1, 4, 2, 5, 6};
        Arrays.sort(arr9);
        System.out.print("Mang sau khi sap xep giam dan:");
        for (int i9 = arr9.length - 1; i9 >= 0; i9--) {
            System.out.print(arr9[i9] + " ");
        }

        //subtask 10 Đổi phần tử từ vị trí thứ x sang vị trí thứ y
        int[] arr10 = {10, 20, 30, 40, 50};
        int x10 = 0;
        int y10 = 2;
        System.out.print("\n" + "Mang ban dau: " + Arrays.toString(arr10) + "\n");
        
        int temp10 = arr10[x10];
        arr10[x10] = arr10[y10];
        arr10[y10] = temp10;
        
        System.out.print("Ket qua sau khi doi vi tri 2 phan tu: ");
        for (int i10 : arr10) {
            System.out.print(i10 + " ");
        }

    }
}
